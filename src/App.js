import Home from "./pages/Home/Home";
import { useSelector, useDispatch } from "react-redux";

function App() {
	return (
		<div className="App">
			<Home />
		</div>
	);
}

export default App;
