import axios from "axios";

const getTodos = () => {
	try {
		return axios
			.get("https://jsonplaceholder.typicode.com/todos")
			.then((response) => {
				return Promise.resolve(response);
			})
			.catch((error) => {
				return Promise.reject(error);
			});
	} catch (err) {
		return Promise.reject(err);
	}
};

export default getTodos;
