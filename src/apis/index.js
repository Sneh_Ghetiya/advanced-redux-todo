import getTodos from "./Get/getTodos";

const apis = {
	get: {
		todos: getTodos,
	},
};

export default apis;
