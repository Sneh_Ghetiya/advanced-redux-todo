import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import actions from "../../store/actions";
import getTodos from "../../apis/Get/getTodos";

const Home = () => {
	const dispatch = useDispatch();
	const handleClick = async () => {
		const data = await dispatch(actions.handlers.getTodosHandler());
	};

	return (
		<div>
			<button onClick={handleClick}>Get Data</button>
		</div>
	);
};

export default Home;
