import { GET_TODOS } from "./types";

export const getTodosAction = () => {
	return {
		type: GET_TODOS,
	};
};
