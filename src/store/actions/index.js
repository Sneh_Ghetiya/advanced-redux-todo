import { GET_TODOS } from "./types";
import { getTodosHandler } from "./actionHandlers";
import { getTodosAction } from "./actionCreators";

const actions = {
	types: {
		GET_TODOS,
	},
	actions: {
		getTodosAction,
	},
	handlers: {
		getTodosHandler,
	},
};

export default actions;
