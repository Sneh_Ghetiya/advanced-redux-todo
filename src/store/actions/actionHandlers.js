import * as ActionCreators from "./actionCreators";
import apis from "../../apis/index";

export const getTodosHandler = () => (dispatch) => {
	return apis.get
		.todos()
		.then((response) => {
			dispatch(ActionCreators.getTodosAction());
			return Promise.resolve(response);
		})
		.catch((err) => {
			return Promise.reject(err);
		});
};
