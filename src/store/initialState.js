const initialState = {
	username: null,
	createdDate: new Date().getDate(),
	todo: {},
	isCompleted: false,
};

export default initialState;
