import initialState from "../initialState";
import actions from "../actions";

const reducer = (state = {}, action) => {
	switch (action.type) {
		case actions.types.GET_TODOS:
			return { ...state };
		default:
			return state;
	}
};

export default reducer;
